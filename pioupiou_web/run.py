from dataclasses import dataclass
import os
from pathlib import Path
from typing import List
from typing import Optional

from aiohttp import web
import aiohttp_jinja2
from hapic import Hapic
from hapic import HapicData
from hapic.error.serpyco import SerpycoDefaultErrorBuilder
from hapic.ext.aiohttp.context import AiohttpContext
from hapic.processor.serpyco import SerpycoProcessor
import jinja2

from pioupiou import AvatarGenerator
from pioupiou.utils import generate_name
from pioupiou_web.themes import generate_default_themes

SAMPLE_DIR_PATH = str(Path(__file__).resolve().parent.parent) + "/sample"
CACHE_DIR = "/tmp/pioupiouweb"
SECRET_TOKEN = "SUPERSECRET"
themes = generate_default_themes(sample_dir_path=SAMPLE_DIR_PATH)
avatar_generator = AvatarGenerator(themes, name=SECRET_TOKEN)


@dataclass
class GetAvatarPath(object):
    hash: str


@dataclass
class GetAvatarParameters(object):
    s: Optional[str] = None  # size
    d: Optional[str] = None  # default_theme


hapic = Hapic(async_=True)
hapic.set_processor_class(SerpycoProcessor)


@dataclass
class ThemeList(object):
    themes: List[str]


@dataclass
class Name(object):
    name: str


async def get_index(request):
    token = generate_name()
    context = {"token": token}
    response = aiohttp_jinja2.render_template("index.html", request, context=context)
    return response


@hapic.with_api_doc()
@hapic.output_body(ThemeList)
async def get_themes_list(request):
    return ThemeList(themes=[theme for theme in avatar_generator.themes.keys()])


@hapic.with_api_doc()
@hapic.output_body(Name)
async def random_name(request):
    return Name(name=generate_name())


@hapic.with_api_doc()
@hapic.input_path(GetAvatarPath)
@hapic.input_query(GetAvatarParameters)
# @hapic.output_file(['image/png'])
async def get_avatar(request, hapic_data: HapicData):
    """
    Obtain avatar from pioupiou
    """
    theme_name = None
    if hapic_data.query.d and hapic_data.query.d in avatar_generator.themes.keys():
        theme_name = hapic_data.query.d
    avatar = avatar_generator.generate_avatar(token=hapic_data.path.hash, theme_name=theme_name)
    if hapic_data.query.s:
        size = int(hapic_data.query.s)
        sizes = (size, size)
        avatar = avatar.resize(sizes)
    file_path = CACHE_DIR + "/{hash}.png".format(hash=hapic_data.path.hash)
    avatar_generator.save_on_disk(avatar, path=file_path)
    return web.FileResponse(file_path)


app = web.Application()
aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(os.path.join(os.getcwd(), "templates")))
app.add_routes(
    [
        web.get(r"/avatar/{hash}.png", get_avatar),
        web.get(r"/avatar/themes_list", get_themes_list),
        web.get(r"/", get_index),
        web.get(r"/random_name", random_name),
    ]
)
context = AiohttpContext(app, default_error_builder=SerpycoDefaultErrorBuilder())
hapic.set_context(context)
hapic.add_documentation_view("/avatar/doc")
web.run_app(app)
