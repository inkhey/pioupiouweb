from pathlib import Path
import typing

from pioupiou.avatar_generator import AvatarGeneratorInterface
from pioupiou.avatar_generator import FolderAvatarTheme


def generate_default_themes(sample_dir_path: str) -> typing.List[AvatarGeneratorInterface]:
    cat_revoy = FolderAvatarTheme(
        folder_path="{}/cat_revoy".format(sample_dir_path),
        layers_name=["body", "fur", "eyes", "mouth", "accessorie"],
        name="cat_revoy",
    )
    bird_revoy = FolderAvatarTheme(
        "{}/bird_revoy".format(sample_dir_path),
        layers_name=["tail", "hoop", "body", "wing", "eyes", "bec", "accessorie"],
        name="bird_revoy",
    )
    monster_id = FolderAvatarTheme(
        "{}/monster_id".format(sample_dir_path),
        layers_name=["legs", "hair", "arms", "body", "eyes", "mouth"],
        name="monster_id",
    )
    return [cat_revoy, bird_revoy, monster_id]


if __name__ == "__main__":
    from pioupiou import AvatarGenerator

    themes = generate_default_themes(sample_dir_path=str(Path(__file__).resolve().parent))
    avatar_generator = AvatarGenerator(themes)
    # note: you can override default theme list and/or theme chooser (mecanism to randomly choose a theme according to token)
    avatar = avatar_generator.generate_avatar(token="just a random string")
    avatar_generator.save_on_disk(avatar, path="/tmp/saved_file.png")

    # note: you can choose a specific theme according to activate theme
    avatar2 = avatar_generator.generate_avatar(
        theme_name="bird_revoy", token="just a random string"
    )
    avatar_generator.save_on_disk(avatar2, path="/tmp/bird.png")
