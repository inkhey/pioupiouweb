# add black for python 3.6+
import os
import sys

from setuptools import find_packages
from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))
try:
    documentation = open(os.path.join(here, "README.md")).read()
except IOError:
    documentation = ""
except UnicodeDecodeError:
    documentation = ""

install_requires = [
    "pioupiou",
    "hapic[serpyco]",
    "aiohttp",
    "jinja2",
    "aiohttp_jinja2"
]
tests_require = ["pytest"]
devtools_require = ["flake8", "isort", "mypy", "pre-commit"]
if sys.version_info.major == 3 and sys.version_info.minor >= 6:
    devtools_require.append("black")
if sys.version_info.major == 3 and sys.version_info.minor <= 7:
    install_requires.append("typing_extensions")

setup(
    name="pioupiou",
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    description="Website for pioupiou avatar generator",
    url="https://framagit.org/inkhey/pioupiou_web",
    long_description=documentation,
    long_description_content_type='text/markdown',
    author="Guénaël Muller",
    author_email="inkey@inkey-art.net",
    keywords=["avatar"],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        'License :: OSI Approved :: MIT License',
    ],
    packages=find_packages(),
    install_requires=install_requires,
    python_requires=">= 3.5",
    include_package_data=True,
    extras_require={"testing": tests_require, "dev": tests_require + devtools_require},
)
