# PiouPiou Web 
![logo](pioupiou.png)

![pipeline_build_status](https://framagit.org/inkhey/pioupiou_web/badges/master/pipeline.svg)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
![mypy](https://img.shields.io/badge/mypy-checked-blueviolet)

A web api and ui for PiouPiou AvatarGenerator.


## Install

### From source

- clone this repository
- `pip install -e "."`

